
package Entidades;

public class Usuarios {
    private int idUsuario;
    private String email;
    private String password;

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }   
    
    
    public String getEmail(){
        return this.email;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public String getPassword() {
        return password;
    }
            
    public Usuarios(int pIdUsuario,String pCorreoElectronico, String pContraseña){
        this.idUsuario=pIdUsuario;
        this.email=pCorreoElectronico;
        this.password=pContraseña;
    }
    
    
}
