package Entidades;

public class Suscripciones {

    private int idSuscripcion;
    private int numeroViajes;
    private int idTarjeta;

    public void Suscripciones() {
    }

    public void Suscripciones(int pIdSuscripcion, int pNumeroViajes) {
        this.idSuscripcion = pIdSuscripcion;
        this.numeroViajes = pNumeroViajes;
    }

    public void Suscripciones(int pIdSuscripcion, int pNumeroViajes, int pIdTarjeta) {
        this.idSuscripcion = pIdSuscripcion;
        this.numeroViajes = pNumeroViajes;
        this.idTarjeta = pIdTarjeta;
    }

    public int getIdSuscripcion() {
        return idSuscripcion;
    }

    public int getNumeroViajes() {
        return numeroViajes;
    }

    public int getIdTarjeta() {
        return idTarjeta;
    }

    public void setIdSuscripcion(int idSuscripcion) {
        this.idSuscripcion = idSuscripcion;
    }

    public void setNumeroViajes(int numeroViajes) {
        this.numeroViajes = numeroViajes;
    }

    public void setIdTarjeta(int idTarjeta) {
        this.idTarjeta = idTarjeta;
    }
}
