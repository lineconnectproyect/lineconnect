
package Entidades;


public class Trabajadores {
    
    private int idTrabajador;
    private String email;
    private String nombre;
    private String apellidos;

    public int getIdUsuario() {
        return idTrabajador;
    }

    public void setIdUsuario(int idUsuario) {
        this.idTrabajador = idUsuario;
    }

    public String getCorreoElectronico() {
        return email;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.email = correoElectronico;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public Trabajadores(int idUsuario, String email, String nombre, String apellidos) {
        this.idTrabajador = idUsuario;
        this.email = email;
        this.nombre = nombre;
        this.apellidos = apellidos;
    }    
    
}
