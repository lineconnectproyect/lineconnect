package Servlets;

import Controladores.SuscripcionesBD;
import Controladores.UsuariosBD;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AdministrarSuscripcion50 extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String page = "";
        String compra = request.getParameter("comprar50");
        int compra2 = Integer.parseInt(compra);
        String servidor = "localhost";
        String baseDatos = "lineconnectdb";
        String usuarioConexion = "root";
        String passwordConexion = "root";
        try {
            int idCliente = UsuariosBD.getUsuarioRegistrado().getIdUsuario();
            SuscripcionesBD controladorSuscripcion = new SuscripcionesBD(servidor, baseDatos, usuarioConexion, passwordConexion);
            controladorSuscripcion.modificarSuscripcion(idCliente, compra2);
        } catch (SQLException ex) {
            page = "error.jsp";
            request.setAttribute("error", ex);
            request.getRequestDispatcher(page).forward(request, response);
        }
        page = "administrarSuscripcion.jsp";
        RequestDispatcher registroExitoso = request.getRequestDispatcher(page);
        registroExitoso.forward(request, response);
    }    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
