/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Controladores.ClientesBD;
import Controladores.SuscripcionesBD;
import Controladores.UsuariosBD;
import Entidades.Clientes;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Usuario
 */
public class RegistrarClienteServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String page = "";
        String nombre = request.getParameter("nombre");
        String apellidos = request.getParameter("apellidos");
        String dni = request.getParameter("dni");
        String email = request.getParameter("email");
        String emailRepeat = request.getParameter("email-repeat");
        int idCliente = 0;
        if (!email.equals(emailRepeat)) {
            page = "error.jsp";
            request.setAttribute("error", "Asegurese que los emails sean iguales");
            request.getRequestDispatcher(page).forward(request, response);
        }
        String password = request.getParameter("password");
        String passwordRepeat = request.getParameter("password-repeat");
        if (!password.equals(passwordRepeat)) {
            page = "error.jsp";
            request.setAttribute("error", "Asegurese que las contraseñas sean iguales");
            request.getRequestDispatcher(page).forward(request, response);
        }
        SimpleDateFormat convertirEnFecha = new SimpleDateFormat("yyyy-mm-dd");
        String fechaNacimiento = request.getParameter("fechaNacimiento");
        String telefono = request.getParameter("telefono");
        String servidor = "localhost";
        String baseDatos = "lineconnectdb";
        String usuarioConexion = "root";
        String passwordConexion = "root";
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            request.setAttribute("error", ex);
            page = "error.jsp";
            request.getRequestDispatcher(page).forward(request, response);
        }
        try {
            UsuariosBD controlUsuario = new UsuariosBD(servidor, baseDatos, usuarioConexion, passwordConexion);
            controlUsuario.registrarUsuario(email, password);
            controlUsuario.iniciarSesion(email,password);
            idCliente = controlUsuario.getUsuarioRegistrado().getIdUsuario();
            ClientesBD controladorCliente = new ClientesBD(servidor, baseDatos, usuarioConexion, passwordConexion);
            controladorCliente.registrarCliente(idCliente, nombre, apellidos, dni, fechaNacimiento,0);
            SuscripcionesBD controladorSuscripcion = new SuscripcionesBD(servidor, baseDatos, usuarioConexion, passwordConexion);
            controladorSuscripcion.suscribirse(idCliente);
            controlUsuario.cerrarSesion();
        } catch (SQLException ex) {
            page = "error.jsp";
            request.setAttribute("error", ex);
            request.getRequestDispatcher(page).forward(request, response);
        }
        page = "identificarse.html";
        RequestDispatcher registroExitoso = request.getRequestDispatcher(page);
        registroExitoso.forward(request, response);

        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet RegistrarClienteServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet RegistrarClienteServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
