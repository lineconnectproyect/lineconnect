package Controladores;

import Entidades.Clientes;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.ArrayList;

public class ClientesBD {

    private static Clientes clienteRegistrado = null;
    private static boolean registroExitoso = false;
    private String usuario = null;
    private String password = null;
    private String baseDatos = null;
    private String servidor = null;

    public static Clientes getClienteRegistrado() {
        return clienteRegistrado;
    }

    public static void setClienteRegistrado(Clientes clienteRegistrado) {
        ClientesBD.clienteRegistrado = clienteRegistrado;
    }

    /**
     * Devuelve true si ha sido posible registrar al usuario
     *
     * @return
     */
    public static boolean isRegistroExitoso() {
        return registroExitoso;
    }

    public static void setRegistroExitoso(boolean registroExitoso) {
        ClientesBD.registroExitoso = registroExitoso;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBaseDatos() {
        return baseDatos;
    }

    public void setBaseDatos(String baseDatos) {
        this.baseDatos = baseDatos;
    }

    public String getServidor() {
        return servidor;
    }

    public void setServidor(String servidor) {
        this.servidor = servidor;
    }

    public String getCadenaConexion() {
        String cadenaConexion = "jdbc:mysql://" + this.servidor + "/" + this.baseDatos + "?serverTimezone=UTC&useSSL=false";
        return cadenaConexion;
    }

    public ClientesBD(String pNombreServidor, String pBaseDatos, String pUsuario, String pContraseña) {
        this.servidor = pNombreServidor;
        this.baseDatos = pBaseDatos;
        this.usuario = pUsuario;
        this.password = pContraseña;
    }

    public void registrarCliente(int pIdUsuario, String pNombre, String pApellidos, String pDNI,
            String pFechaNacimiento, int pTelefono)
            throws SQLException {
        String consulta = "insert into clientes (idCliente,nombre,apellidos,dni,fechaNacimiento,telefono) values(?,?,?,?,?,?)";
        String cadenaConexion = getCadenaConexion();
        try (Connection conexion = DriverManager.getConnection(cadenaConexion, this.usuario, this.password)) {
            PreparedStatement insertarCliente = conexion.prepareStatement(consulta);
            insertarCliente.setInt(1, pIdUsuario);
            insertarCliente.setString(2, pNombre);
            insertarCliente.setString(3, pApellidos);
            insertarCliente.setString(4, pDNI);
            insertarCliente.setString(5, pFechaNacimiento);
            insertarCliente.setInt(6, pTelefono);
            insertarCliente.executeUpdate();
            this.registroExitoso = true;
        }
    }

    public void obtenerPerfil() throws SQLException {

        String cadenaConexion = getCadenaConexion();
        String consulta = "select*from clientes";
        int idCliente = UsuariosBD.getUsuarioRegistrado().getIdUsuario();

        try (Connection conexion = DriverManager.getConnection(cadenaConexion, this.usuario, this.password)) {
            Statement seleccionarCliente = conexion.createStatement();
            ResultSet clientesBD = seleccionarCliente.executeQuery(consulta);
            while (clientesBD.next()) {
                if (clientesBD.getInt("idCliente") == idCliente) {
                    String dni = clientesBD.getString("dni");
                    String nombre = clientesBD.getString("nombre");
                    String apellidos = clientesBD.getString("apellidos");
                    int telefono = clientesBD.getInt("telefono");
                    String fechaNacimiento = clientesBD.getString("fechaNacimiento");
                    clienteRegistrado = new Clientes(idCliente, dni, nombre, apellidos, telefono, fechaNacimiento);
                }
            }
        }
    }

    public void modificarCliente(int pIdUsuario, String pNombre, String pApellidos, String pDNI, String pEmail,
            String pFechaNacimiento, int pTelefono)
            throws SQLException {
        String consulta = "update clientes set nombre=?, apellidos=?, dni=?, fechaNacimiento=?, telefono=? where idCliente=?";
        String cadenaConexion = getCadenaConexion();
        try (Connection conexion = DriverManager.getConnection(cadenaConexion, this.usuario, this.password)) {
            PreparedStatement actualizarCampo = conexion.prepareStatement(consulta);
            actualizarCampo.setString(1, pNombre);
            actualizarCampo.setString(2, pApellidos);
            actualizarCampo.setString(3, pDNI);
            actualizarCampo.setString(4, pFechaNacimiento);
            actualizarCampo.setInt(5, pTelefono);
            actualizarCampo.setInt(6, pIdUsuario);
            actualizarCampo.executeUpdate();
            ClientesBD.clienteRegistrado=new Clientes(pIdUsuario,pDNI,pNombre,pApellidos,pTelefono,pFechaNacimiento);
            String emailActual = UsuariosBD.getUsuarioRegistrado().getEmail();
            if (!pEmail.equals(emailActual)) {
                consulta = "update usuarios set email=? where idUsuario=?";
                actualizarCampo = conexion.prepareStatement(consulta);
                actualizarCampo.setString(1, pEmail);
                actualizarCampo.setInt(2, pIdUsuario);
                actualizarCampo.executeUpdate();
                UsuariosBD.getUsuarioRegistrado().setEmail(pEmail);
            }
        }
    }
}
