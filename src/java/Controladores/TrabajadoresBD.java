
package Controladores;
import Entidades.Trabajadores;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class TrabajadoresBD {
    
    private static Trabajadores trabajadorRegistrado=null;
    private String usuario=null;
    private String contraseña=null;
    private String baseDatos=null;
    private String servidor=null;

    public static Trabajadores getTrabajadorRegistrado() {
        return trabajadorRegistrado;
    }

    public static void setTrabajadorRegistrado(Trabajadores trabajadorRegistrado) {
        TrabajadoresBD.trabajadorRegistrado = trabajadorRegistrado;
    }
    
    public String getCadenaConexion(){
        String cadenaConexion="jdbc:mysql://"+this.servidor+"/"+this.baseDatos+"?serverTimezone=UTC&useSSL=false";
        return cadenaConexion;
    }

    public TrabajadoresBD(String pNombreServidor,String pBaseDatos, String pUsuario, String pContraseña) {
        
        this.servidor=pNombreServidor;
        this.baseDatos=pBaseDatos;
        this.usuario=pUsuario;
        this.contraseña=pContraseña;
    }   
    
    public ResultSet listaClientesYSuscripciones()throws SQLException{
        
        String cadenaConexion=getCadenaConexion();
        String seleccion="select idCliente,dni,nombre,apellidos,telefono,fechaNacimiento,numero_viajes,idTargeta,email,password";
        String primeraUnionTablas="usuarios u join clientes c on u.idUsuario=c.idCliente";
        String segundaUnionTablas="clientes c join suscripciones s on c.idCliente=s.idsuscripcion";
        StringBuilder consulta=new StringBuilder(seleccion);
        consulta.append(primeraUnionTablas);
        consulta.append(segundaUnionTablas);
        try(Connection conexion=DriverManager.getConnection(cadenaConexion,this.usuario,this.contraseña)){
            Statement seleccionarClientesYSuscripciones=conexion.createStatement();
            ResultSet selectos=seleccionarClientesYSuscripciones.executeQuery(consulta.toString());
            return selectos;
        }
    }
    
    /**
       * Genera el objeto trabajadorRegistrado, con los datos obtenidos de la base de datos, para mantener la sesión iniciada.
       * Sí el objeto queda con un valor "null", es porque no ha sido encontrado el "usuario" en "trabajadores".
    **/
    public void obtenerPerfil()throws SQLException{
        
        String cadenaConexion=getCadenaConexion();
        String consulta="select*from trabajadores";
        int idTrabajador=UsuariosBD.getUsuarioRegistrado().getIdUsuario();
        
        try(Connection conexion=DriverManager.getConnection(cadenaConexion,this.usuario,this.contraseña)){
            Statement seleccionarTrabajador=conexion.createStatement();
            ResultSet trabajadoresBD=seleccionarTrabajador.executeQuery(consulta);
            while(trabajadoresBD.next()){
                if(trabajadoresBD.getInt("idTrabajador")==idTrabajador){
                    String dni=trabajadoresBD.getString("dni");
                    String nombre=trabajadoresBD.getString("nombre");
                    String apellidos=trabajadoresBD.getString("apellidos");
                    String correoElectronico=trabajadoresBD.getString("email");
                    trabajadorRegistrado=new Trabajadores(idTrabajador,correoElectronico,nombre,apellidos);
                    break;
                }
            }
        }
        
    }
    
}
