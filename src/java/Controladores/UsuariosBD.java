package Controladores;

import Entidades.Usuarios;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.ResultSet;

public class UsuariosBD {

    private static Usuarios usuarioRegistrado = null;
    private String usuario = null;
    private String password = null;
    private String baseDatos = null;
    private String servidor = null;
    private static boolean sesionIniciada = false;

    /**
     * Genera el objeto usuarioRegistrado, para mantener la sesión abierta
     *
     */
    public static boolean isSesionIniciada() {
        return sesionIniciada;
    }

    public void setSesionIniciada(boolean sesionIniciada) {
        this.sesionIniciada = sesionIniciada;
    }

    public static Usuarios getUsuarioRegistrado() {
        return usuarioRegistrado;
    }

    public static void setUsuarioRegistrado(Usuarios usuarioRegistrado) {
        UsuariosBD.usuarioRegistrado = usuarioRegistrado;
    }

    public String getCadenaConexion() {
        String cadenaConexion = "jdbc:mysql://" + this.servidor + "/" + this.baseDatos + "?serverTimezone=UTC&useSSL=false";
        return cadenaConexion;
    }

    public UsuariosBD(String pNombreServidor, String pBaseDatos, String pUsuario, String pPassword) {
        this.servidor = pNombreServidor;
        this.baseDatos = pBaseDatos;
        this.usuario = pUsuario;
        this.password = pPassword;
    }

    public void registrarUsuario(String pEmail, String pPassword) throws SQLException {

        String cadenaConexion = getCadenaConexion();
        String consulta = "insert into usuarios (email,password) values(?,?)";

        try (Connection conexion = DriverManager.getConnection(cadenaConexion, this.usuario, this.password)) {

            PreparedStatement insertarUsuario = conexion.prepareStatement(consulta);
            insertarUsuario.setString(1, pEmail);
            insertarUsuario.setString(2, pPassword);
            insertarUsuario.execute();
        }

    }

    public void modificarUsuario(int pIdUsuario, String pEmailNuevo, String pPasswordNueva) throws SQLException {
        Usuarios usuarioActual = UsuariosBD.getUsuarioRegistrado();
        String emailActual = usuarioActual.getEmail();
        String passwordActual = usuarioActual.getPassword();
        String cadenaConexion = getCadenaConexion();

        try (Connection conexion = DriverManager.getConnection(cadenaConexion, this.usuario, this.password)) {
            if (!pEmailNuevo.equals(emailActual)) {

                String consulta = "update usuarios set email=? where idUsuario=?";
                PreparedStatement actualizarEmail = conexion.prepareStatement(consulta);
                actualizarEmail.setString(1, pEmailNuevo);
                actualizarEmail.setInt(2, pIdUsuario);
                actualizarEmail.executeUpdate();
                UsuariosBD.getUsuarioRegistrado().setEmail(pEmailNuevo);
            }
            if (!passwordActual.equals(pPasswordNueva)) {

                String consulta = "update usuarios set password=? where idUsuario=?";
                PreparedStatement actualizarContraseña = conexion.prepareStatement(consulta);
                actualizarContraseña.setString(1, pPasswordNueva);
                actualizarContraseña.setInt(2, pIdUsuario);
                actualizarContraseña.executeUpdate();
                UsuariosBD.getUsuarioRegistrado().setPassword(pPasswordNueva);
            }
        }
    }

    public void modificarEmail(int pIdUsuario, String pEmailNuevo) throws SQLException {
        String emailActual = UsuariosBD.getUsuarioRegistrado().getEmail();
        if (!pEmailNuevo.equals(emailActual)) {
            String consulta = "update usuarios set email=? where idUsuario=?";
            String cadenaConexion = getCadenaConexion();
            try (Connection conexion = DriverManager.getConnection(cadenaConexion, this.usuario, this.password)) {
                PreparedStatement cambiarEmail = conexion.prepareStatement(consulta);
                cambiarEmail.setString(1, pEmailNuevo);
                cambiarEmail.setInt(2, pIdUsuario);
                cambiarEmail.executeUpdate();
                UsuariosBD.getUsuarioRegistrado().setEmail(pEmailNuevo);
            }
        }
    }

    public void iniciarSesion(String pEmail, String pPassword) throws SQLException {

        String cadenaConexion = getCadenaConexion();
        String consulta = "select * from usuarios";
        ResultSet sesiones = null;

        try (Connection conexion = DriverManager.getConnection(cadenaConexion, this.usuario, this.password)) {
            PreparedStatement seleccionar = conexion.prepareStatement(consulta);
            sesiones = seleccionar.executeQuery();
            while (sesiones.next()) {
                if (pEmail.equals(sesiones.getString("email"))) {
                    if (pPassword.equals(sesiones.getString("password"))) {
                        usuarioRegistrado = new Usuarios(sesiones.getInt("idUsuario"), pEmail, pPassword);
                        this.sesionIniciada = true;
                        break;
                    } else {
                        this.sesionIniciada = false;
                        break;
                    }
                }
            }
        }
    }

    public void cerrarSesion() {
        usuarioRegistrado = null;
    }

}
