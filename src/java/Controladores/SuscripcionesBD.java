package Controladores;

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.DriverManager;
import Entidades.Suscripciones;

public class SuscripcionesBD {

    private static Suscripciones suscripcion = null;
    private String usuario = null;
    private String contraseña = null;
    private String baseDatos = null;
    private String servidor = null;

    public static Suscripciones getSuscripcion() {
        return suscripcion;
    }

    public static void setSuscripcion(Suscripciones suscripcion) {
        SuscripcionesBD.suscripcion = suscripcion;
    }

    public String getCadenaConexion() {
        String cadenaConexion = "jdbc:mysql://" + this.servidor + "/" + this.baseDatos + "?serverTimezone=UTC&useSSL=false";
        return cadenaConexion;
    }

    public SuscripcionesBD(String pNombreServidor, String pBaseDatos, String pUsuario, String pContraseña) {
        this.servidor = pNombreServidor;
        this.baseDatos = pBaseDatos;
        this.usuario = pUsuario;
        this.contraseña = pContraseña;
    }

    public void suscribirse(int pIdUsuario) throws SQLException {
        String cadenaConexion = getCadenaConexion();
        String consulta = "insert into suscripciones (idsuscripcion,numero_viajes) values(?,?)";
        try (Connection conexion = DriverManager.getConnection(cadenaConexion, this.usuario, this.contraseña)) {
            PreparedStatement insertarSuscripcion = conexion.prepareStatement(consulta);
            insertarSuscripcion.setInt(1, pIdUsuario);
            insertarSuscripcion.setInt(2, 0);
            insertarSuscripcion.executeUpdate();
            this.suscripcion = new Suscripciones();
            this.suscripcion.setIdSuscripcion(pIdUsuario);
            this.suscripcion.setNumeroViajes(0);
        }
    }

    public void modificarSuscripcion(int pIdUsuario, int viajes) throws SQLException {
        String cadenaConexion = getCadenaConexion();
        String consulta = "Update suscripciones set numero_viajes=? where idsuscripcion=?";
        try (Connection conexion = DriverManager.getConnection(cadenaConexion, this.usuario, this.contraseña)) {
            PreparedStatement agregarViajes = conexion.prepareStatement(consulta);
            agregarViajes.setInt(2, pIdUsuario);
            agregarViajes.setInt(1, viajes);
            agregarViajes.executeUpdate();
            this.suscripcion = new Suscripciones();
            this.suscripcion.setIdSuscripcion(pIdUsuario);
            this.suscripcion.setNumeroViajes(viajes);
        }
    }

    public void bajaSuscripcion(String pDNI) throws SQLException {
        String cadenaConexion = getCadenaConexion();
        String consulta = "delete from suscripciones where dni=?";
        try (Connection conexion = DriverManager.getConnection(cadenaConexion, this.usuario, this.contraseña)) {
            PreparedStatement eliminarSuscripcion = conexion.prepareStatement(consulta);
            eliminarSuscripcion.setString(1, pDNI);
            eliminarSuscripcion.executeUpdate();
            this.suscripcion = null;
        }
    }
}
