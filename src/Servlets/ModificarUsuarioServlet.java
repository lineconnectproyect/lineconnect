/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.RequestDispatcher;

import Controladores.ClientesBD;
import java.sql.SQLException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import Entidades.Clientes;

/**
 *
 * @author Usuario
 */
@WebServlet(name = "ModificarUsuarioServlet", urlPatterns = {"/ModificarUsuarioServlet"})
public class ModificarUsuarioServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        String page="";
        String nombre=request.getParameter("nombre");
        String apellidos=request.getParameter("apellidos");
        String dni=request.getParameter("dni");
        String email=request.getParameter("email");
        String emailRepeat=request.getParameter("email-repeat");
        if(!email.equals(emailRepeat)){
            page="error.jsp";
            request.setAttribute("error","Asegurese que los emails sean iguales");
            request.getRequestDispatcher(page).forward(request,response);
        }
        String password=request.getParameter("password");
        String passwordRepeat=request.getParameter("password-repeat");
        if(!password.equals(passwordRepeat)){
            page="error.jsp";
            request.setAttribute("error","Asegurese que las contraseñas sean iguales");
            request.getRequestDispatcher(page).forward(request,response);
        }
        SimpleDateFormat convertirEnFecha=new SimpleDateFormat("yyyy-mm-dd");
        Date fechaNacimiento=null;
        try{
            String fechaString=request.getParameter("fechaNacimiento");
            fechaNacimiento=(Date) convertirEnFecha.parse(fechaString);
        }catch(ParseException ex){
            request.setAttribute("error","Asegurese de introducir la fecha en el formato correcto yyyy-mm-dd");
            page="error.jsp";
            request.getRequestDispatcher(page).forward(request, response);
        }
        String telefono=request.getParameter("telefono");
        
        Clientes cliente=ClientesBD.getClienteRegistrado();
        String servidor="localhost";
        String baseDatos="lineconnectbd";
        String usuarioConexion="root";
        String passwordConexion="root";
        
        try{
            int idCliente=cliente.getIdCliente();
            ClientesBD controladorCliente=new ClientesBD(servidor,baseDatos,usuarioConexion,passwordConexion);
            controladorCliente.registrarCliente(idCliente,nombre,apellidos,dni,email,fechaNacimiento,telefono);
            request.setAttribute("nombre",cliente.getNombre());
            request.setAttribute("apellidos",cliente.getApellidos());
            request.setAttribute("dni",cliente.getDni());
            request.setAttribute("email",cliente.getEmail());
            request.setAttribute("fechaNacimiento",cliente.getFechaNacimiento());
            request.setAttribute("telefono",cliente.getTelefono());
        }catch(SQLException ex){
            page="error.jsp";
            request.setAttribute("error",ex);
            request.getRequestDispatcher(page).forward(request,response);
        }
        page="reqistrarse.html";
        RequestDispatcher registroExitoso=request.getRequestDispatcher(page);
        registroExitoso.forward(request,response);
        
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ModificarUsuarioServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ModificarUsuarioServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
