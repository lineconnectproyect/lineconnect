package Servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.RequestDispatcher;

import java.sql.SQLException;

import Controladores.UsuariosBD;
import Controladores.ClientesBD;
import Controladores.TrabajadoresBD;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebServlet(name = "IniciarSesion", urlPatterns = {"/IniciarSesion"})
public class IniciarSesionServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        String correoElectronico = request.getParameter("email");
        String contraseña = request.getParameter("password");
        String esCliente = request.getParameter("esCliente");
        String pagina = null;

        String servidor = "localhost";
        String baseDatos = "lineconnectdb";
        String usuarioConexion = "root";
        String contraseñaConexion = "root";

        UsuariosBD controladorUsuario = new UsuariosBD(servidor, baseDatos, usuarioConexion, contraseñaConexion);
        String nombreUsuario = null;
        Boolean usuarioCorrecto = false;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            controladorUsuario.iniciarSesion(correoElectronico, contraseña);
            usuarioCorrecto = UsuariosBD.isSesionIniciada();
        } catch (SQLException | ClassNotFoundException ex) {
            request.setAttribute("error", ex.toString());
            request.getRequestDispatcher("error.jsp").forward(request, response);
        }
        if (usuarioCorrecto == false) {
            pagina = "error.html";
            request.setAttribute("error", "El usuario o la contraseña no han sido encontrados en la base de datos.");
            request.getRequestDispatcher(pagina).forward(request, response);
        } else {
            if (esCliente.equals("no")) {
                TrabajadoresBD controladorTrabajador = new TrabajadoresBD(servidor, baseDatos, usuarioConexion, contraseñaConexion);
                try {
                    controladorTrabajador.obtenerPerfil();
                } catch (SQLException ex) {
                    request.setAttribute("usuario", ex.toString());
                    request.getRequestDispatcher("error.jsp").forward(request, response);
                }
                nombreUsuario = TrabajadoresBD.getTrabajadorRegistrado().getNombre();
                pagina = "error.jsp";
            } else {
                ClientesBD controladorCliente = new ClientesBD(servidor, baseDatos, usuarioConexion, contraseñaConexion);
                try {
                    controladorCliente.obtenerPerfil();
                } catch (SQLException ex) {
                    request.setAttribute("error", ex.toString());
                    request.getRequestDispatcher("error.jsp").forward(request, response);

                }
                nombreUsuario = ClientesBD.getClienteRegistrado().getNombre();
                pagina = "registrarse.html";
            }
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher(pagina);
        request.setAttribute("usuario", nombreUsuario);
        dispatcher.forward(request, response);

        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet IniciarSesion</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet IniciarSesion at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
