<%-- 
    Document   : modificarCliente
    Created on : 02-dic-2019, 15:44:54
    Author     : Usuario
--%>

<%@page import="Controladores.ClientesBD"%>
<%@page import="Entidades.Usuarios"%>
<%@page import="Entidades.Clientes"%>
<%@page import="Controladores.UsuariosBD"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <title>Line Connect</title>
        <link rel="stylesheet" href="styles/css/bootstrap.min.css">
        <link rel="stylesheet" href="styles/styles.css">
        <link rel="stylesheet" href="styles/registrarse.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    </head>
    <body>
        <header>
            <div class="jumbotron jumbotron-fluid" style="margin-bottom: 0px;">
                <h1 class="display-4" >Line Connect</h1>
                <p class="lead">La herramienta que te ayuda a gestionar el transporte</p>
            </div>

            <!--Navbar -->
            <nav class="mb-1 navbar navbar-expand-lg navbar-dark">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-333"
                        aria-controls="navbarSupportedContent-333" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent-333">

                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="index.html">Inicio</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="identificarse.html">Identificarse</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="registrarse.html">Registrarse</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Contacto</a>
                        </li>
                    </ul>

                    <ul class="navbar-nav ml-auto nav-flex-icons">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-333" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-user"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-default" aria-labelledby="navbarDropdownMenuLink-333">
                                <a class="dropdown-item" href="#">Ver Perfil</a>
                                <a class="dropdown-item" href="#">Salir</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
            <!--Navbar -->
        </header>
        <%Usuarios usuarioIniciado = UsuariosBD.getUsuarioRegistrado();%> 
        <%Clientes clienteIniciado = ClientesBD.getClienteRegistrado();%>

        <%String nombre = clienteIniciado.getNombre();%>
        <%String apellido = clienteIniciado.getApellidos();%>
        <%String DNI = clienteIniciado.getDni();%>
        <%String correoElectronico = usuarioIniciado.getEmail();%>
        <%String fechaNacimiento = clienteIniciado.getFechaNacimiento();%>
        <%int telefono = clienteIniciado.getTelefono();%>
        <Section class="row">
            <form class="modal-content" action="ModificarClienteServlet" method="post">
                <div class="container" >
                    <h1>Datos Personales</h1>
                    <p>Ingrese los datos que desee modificar</p>
                    <hr>          

                    <label for="email"><b>Nombre</b></label>
                    <input type="text" value=<%=nombre%> name="nombre" >

                    <label for="email"><b>Apellido</b></label>
                    <input type="text" value=<%=apellido%> name="apellidos" >

                    <label for="email"><b>DNI</b></label>
                    <input type="text" value=<%=DNI%> name="dni" pattern=".{9}" >

                    <label for="email"><b>Correo electrónico</b></label>
                    <input type="text" value=<%=correoElectronico%> name="email" >


                    <label for="email"><b>Fecha Nacimiento</b></label>
                    <input type="text" value=<%=fechaNacimiento%> name="fechaNacimiento" >

                    <label for="email"><b>Telefono</b></label>
                    <input type="text" value=<%=telefono%> name="telefono" >




                    <div class="clearfix">
                        <button type="button" onclick="document.getElementById('id01').style.display = 'none'" class="cancelbtn">Cancel</button>
                        <button type="submit" class="signupbtn">Actualizar</button>
                    </div>
                </div>
            </form>
        </Section>  



        <!-- Footer -->
        <footer class="page-footer font-small cyan darken-3">

            <div class="container">
                <div class="row">
                    <div class="col-md-12 py-5">
                        <div class="mb-5 flex-center">

                            <!-- Facebook -->
                            <a class="fb-ic">
                                <i class="fab fa-facebook-f fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                            </a>

                            <!-- Twitter -->
                            <a class="tw-ic">
                                <i class="fab fa-twitter fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                            </a>

                            <!-- Google +-->
                            <a class="gplus-ic">
                                <i class="fab fa-google-plus-g fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                            </a>

                            <!--Linkedin -->
                            <a class="li-ic">
                                <i class="fab fa-linkedin-in fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                            </a>

                            <!--Instagram-->
                            <a class="ins-ic">
                                <i class="fab fa-instagram fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                            </a>

                            <!--Pinterest-->
                            <a class="pin-ic">
                                <i class="fab fa-pinterest fa-lg white-text fa-2x"> </i>
                            </a>

                        </div>
                    </div>
                </div>
            </div>

            <!-- Copyright -->
            <div class="footer-copyright text-center py-3">© 2019 Copyright:
                <a href="index.html"> lineconnect.com</a>
            </div>
            <!-- Copyright -->
        </footer>



        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>
